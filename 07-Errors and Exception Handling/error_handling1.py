def ask_for_int():

    while True:
        try:
            result = int(input("Please enter a number: "))
        except:
            print('Whoops! wrong entry..')
            continue
        else:
            print('Yes, thank you.')
            break
        #finally:
        #    print('End of Try/Except!!')

    return result

x = ask_for_int()
print(x)
