"""Handle the exception thrown by the code below by using try and except blocks."""

try:
    for i in ['a','b','c']:
        print(i**2)
except:
    print('Some values are strings.')



"""Handle the exception thrown by the code below by using try and except blocks. Then use a finally block to print 'All Done.'"""

try:
    x = 5
    y = 0

    z = x/y

except:
    if y == 0:
        print('Your answer would be Infinty..')

finally:
    print('All Done')


"""Write a function that asks for an integer and prints the square of it. Use a while loop with a try, except, else block to account for incorrect inputs."""

def ask():

    while True:
        try:
            x = int(input('Please enter a number: '))
        except:
            print('Input is not an integer!')
            continue
        else:
            break
    return x**2

print(ask())
