from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"


# $ FLASK_APP=hello.py flask run
 # * Running on http://localhost:5000/

"""
set FLASK_APP=flaskr
set FLASK_ENV=development
flask init-db
flask run
"""
