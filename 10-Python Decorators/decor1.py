def func():
    return 1

func2 = func
func2()

func()

def hello(name='Jose'):

    def greet():
        return '\t This is inside the greet() function'

    def welcome():
        return "\t This is inside the welcome() function"

    if name == 'Jose':
        return greet
    else:
        return welcome


myfunc = hello('Jose')
myfunc = hello('Josaa')

print(myfunc())

## Return a Function
def cool():

    def supercool():
        return 'I am cool'

    return supercool

somefunc= cool()
print(somefunc())

def hello():
    return 'Hi Jose!'

## Function as an argument
def other(some_def_func):
    print('Other code runs here')
    print(some_def_func())

other(hello)


def new_decorator(original_func):

    def wrap_func():
        print('Some extra code before the original function')

        original_func()

        print('Some extra code, after the original function execution!')

    return wrap_func


def func_needs_decorator():
    print("I want to be contained!")


func_needs_decorator()
decorated = new_decorator(func_needs_decorator)
decorated()
