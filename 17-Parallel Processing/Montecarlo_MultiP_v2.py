"""Multiprocessing Example: Monte Carlo

Let's code out an example to see how the parts fit together. We can time our results using the timeit module to measure any performance gains. Our task is to apply the Monte Carlo Method to estimate the value of Pi.

If you draw a circle of radius 1 (a unit circle) and enclose it in a square, the areas of the two shapes are given as
Area Formulas circle
πr2
square
4r2

Therefore, the ratio of the volume of the circle to the volume of the square is
π4

The Monte Carlo Method plots a series of random points inside the square. By comparing the number that fall within the circle to those that fall outside, with a large enough sample we should have a good approximation of Pi. """


"""Next we'll write a script that sets up a pool of workers, and lets us time the results against varying sized pools. We'll set up two arguments to represent processes and total_iterations. Inside the script, we'll break total_iterations down into the number of iterations passed to each process, by making a processes-sized list.
For example:

total_iterations = 1000
processes = 5
iterations = [total_iterations//processes]*processes
iterations
# Output: [200, 200, 200, 200, 200]   """

from random import random
from multiprocessing import Pool
import timeit
import sys


N = int(sys.argv[1])  # these arguments are passed in from the command line
P = int(sys.argv[2])

if not N or not P:
    print('Please specify Iterations and number of Processes ..')
    sys.quit()

def find_pi(n):
    """
    Function to estimate the value of Pi
    """
    inside=0

    for i in range(0,n):
        x=random()
        y=random()
        if (x*x+y*y)**(0.5)<=1:  # if i falls inside the circle
            inside+=1

    pi=4*inside/n
    return pi

if __name__ == '__main__':
    with Pool(P) as p:
        print(timeit.timeit(lambda: print(f'{sum(p.map(find_pi, [N//P]*P))/P:0.5f}'), number=10))
    print(f'{N} total iterations with {P} processes')
