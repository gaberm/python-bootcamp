#!/usr/bin/env python

"""Using queues with threads

As I referred to earlier, threading can be complicated when threads need to share data or resources. The threading module does provide many synchronization primatives, including semaphores, condition variables, events, and locks. While these options exist, it is considered a best practice to instead concentrate on using queues. Queues are much easier to deal with, and make threaded programming considerably safer, as they effectively funnel all access to a resource to a single thread, and allow a cleaner and more readible design pattern.

In the next example, you will first create a program that will serially, or one after the other, grab a URL of a website, and print out the first 1024 bytes of the page. This is a classic example of something that could be done quicker using threads. First, let's use the urllib2 module to grab these pages one at a time, and time the code: """

import urllib.request
import time

hosts = ["http://yahoo.com", "http://google.com",
         "http://amazon.com", "http://ibm.com", "http://apple.com"]
start = time.time()


# grabs urls of hosts and prints first 1024 bytes of page
for host in hosts:
    url = urllib.request.urlopen(host)
    print( url.read(1024))

print( "Elapsed Time: %s" % (time.time() - start))
