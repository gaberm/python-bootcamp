# http://www.drdobbs.com/open-source/concurrency-and-python/206103078?pgno=3
"""using generators to call co-routines"""
import itertools


def my_coro(name):
    count = 0
    while True:
        count += 1
        print( "%s %s" % (name, count))
        yield


coros = [my_coro('coro1'), my_coro('coro2')]
counter = 0
for coro in itertools.cycle(coros):  # A round-robin scheduler :)
    # coro.__next__()
    next(coro)
    counter +=1
    if counter == 20:
        break
