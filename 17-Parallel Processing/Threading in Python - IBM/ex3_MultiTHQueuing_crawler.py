#!/usr/bin/env python

"""Using queues with threads

As I referred to earlier, threading can be complicated when threads need to share data or resources. The threading module does provide many synchronization primatives, including semaphores, condition variables, events, and locks. While these options exist, it is considered a best practice to instead concentrate on using queues. Queues are much easier to deal with, and make threaded programming considerably safer, as they effectively funnel all access to a resource to a single thread, and allow a cleaner and more readible design pattern.

In the next example, you will first create a program that will serially, or one after the other, grab a URL of a website, and print out the first 1024 bytes of the page. This is a classic example of something that could be done quicker using threads. First, let's use the urllib.request module to grab these pages one at a time, and time the code: """

import queue
import threading
import urllib.request
import time
# urllib.disable_warnings(urllib.request.exceptions.InsecureRequestWarning)


queue = queue.Queue()
hosts = ["http://yahoo.com", "http://google.com",
         "http://amazon.com", "http://ibm.com", "http://apple.com"]
start = time.time()


class ThreadUrl(threading.Thread):
    """Threaded Url Grab"""

    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # grabs host from queue
            host = self.queue.get()

            # grabs urls of hosts and prints first 1024 bytes of page
            url = urllib.request.urlopen(host)
            print( url.read(1024))

            # signals to queue job is done
            self.queue.task_done()

start = time.time()


def main():

        # spawn a pool of threads, and pass them queue instance
    for i in range(4):
        t = ThreadUrl(queue)
        t.setDaemon(True)
        t.start()

    # populate queue with data
    for host in hosts:
        queue.put(host)

    # wait on the queue until everything has been processed
    queue.join()


main()
print("Elapsed Time: %s" % (time.time() - start))
