#/usr/bin/env python3.7
def create_cubes(n):
    result = []
    for x in range(n):
        result.append(x**3)
    return result

create_cubes(10)
for x in create_cubes(10):
    print(x)

def create_cubes(n):
    ## Generator example
    for x in range(n):
        yield x**3

for x in create_cubes(10000):
    print(x)

create_cubes(10000)
print(list(create_cubes(10000)))


def mem_fib(n, _cache={}):
    '''efficiently memoized recursive function, returns a Fibonacci number'''
    if n in _cache:
        return _cache[n]
    elif n > 1:
        return _cache.setdefault(n, mem_fib(n-1) + mem_fib(n-2))
    return n

for i in range(1000):
    print(mem_fib(i))

def genfib(n):
    a,b = 0,1
    for i in range(n):
        a,b = b, a+b
        # # print(a,",",b)
        yield a
        # a = b
        # b = a+b
        # b = a


print(list(genfib(10)))

for i in genfib(1000):
    print(i)


def genfibon(n):
    a,b = 1,1
    output = []

    for i in range(n):
        output.append(a)
        a,b = b,a+b
    return output

genfibon(10)


def simple_gen():
    for x in range(3):
        yield x

list(simple_gen())

for x in simple_gen():
    print( x )

g = simple_gen()
print(next(g))

## Or try to catch exception..
g = simple_gen()
while True:
    try:
        print( next(g) )
    except StopIteration:
        break

## Iter over non iterable variables..
s = 'hello'
s_iter = iter(s)
next(s_iter)


def myGenerator():
    yield 'These'
    yield 'words'
    yield 'come'
    yield 'one'
    yield 'at'
    yield 'a'
    yield 'time'

myGeneratorInstance = myGenerator()
next(myGeneratorInstance)
