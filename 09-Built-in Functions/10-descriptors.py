import collections

collections.__dict__
collections.__name__
collections.__doc__
collections.Counter.__doc__
collections.Counter.__get__(object) #is a method that retrieves the attribute value from object.
collections.Counter.__set__(object, value) #sets the attribute on object to value.
collections.Counter.__delete__(object, value) #deletes the value attribute of object.

# A recursive generator that generates Tree leaves in in-order.
def inorder(t):
    if t:
        for x in inorder(t.left):
            yield x
        yield t.label
        for x in inorder(t.right):
            yield x

x = inorder([1, 2, 4, 6, 8, 10])
next(x)

def generate_ints(N):
    for i in range(N):
        yield i

list(generate_ints(10))

for i in generate_ints(5):
    print(i)

a,b,c = generate_ints(3)

sentence := "Store it in the neighboring harbor"
if (i := find("or", sentence)) > 5 then write(i)
