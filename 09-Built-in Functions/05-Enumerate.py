""" enumerate(). Enumerate allows you to keep a count as you iterate through an object. It does this by returning a tuple in the form (count,element). The function itself is equivalent to:

def enumerate(sequence, start=0):
    n = start
    for elem in sequence:
        yield n, elem
        n += 1

"""

lst = ['a','b','c']
for number,item in enumerate(lst):
    print(number)
    print(item)


# enumerate() becomes particularly useful when you have a case where you need to have some sort of tracker. For example:

for count,item in enumerate(lst):
    if count >= 2:
        break
    else:
        print(item)

for item in lst[:2]:
    print(item)

# enumerate() takes an optional "start" argument to override the default value of zero:
months = ['March','April','May','June']
list(enumerate(months,start=3))

count = 2
for item in months:
    count +=1
    print( (item, count))
    a =  (item, count)
