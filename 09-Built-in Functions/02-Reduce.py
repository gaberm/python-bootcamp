"""reduce()

If seq = [ s1, s2, s3, ... , sn ], calling reduce(function, sequence) works like this:

    At first the first two elements of seq will be applied to function, i.e. func(s1,s2)
    The list on which reduce() works looks now like this: [ function(s1, s2), s3, ... , sn ]
    In the next step the function will be applied on the previous result and the third element of the list, i.e. function(function(s1, s2),s3)
    The list looks like this now: [ function(function(s1, s2),s3), ... , sn ]
    It continues like this until just one element is left and return this element as the result of reduce()
"""

from functools import reduce

lst =[47,11,42,13]

from IPython.display import Image
Image('http://www.python-course.eu/images/reduce_diagram.png')
reduce(lambda x,y: x+y,lst)

reduce(lambda x,y: x*y, lst)


# Factorial using reduce..
lst2 =[]
for i in range(1,5):
    lst2.append(i)
reduce(lambda x,y: x*y, lst2)

reduce(lambda x,y: x*y, [x for x in range(1,5)])

#Find the maximum of a sequence (This already exists as max())
reduce (lambda a,b: a if (a>b) else b, lst)

x =lambda a,b: ( (b[0]-a[0])**2 + (b[1] -a[0])**2 )**(1/2)
reduce (lambda a,b: a if (a>b) else b, lst)


"""reduce() vs accumulate()

Both reduce() and accumulate() can be used to calculate the summation of a sequence elements. But there are differences in the implementation aspects in both of these.

    reduce() is defined in “functools” module, accumulate() in “itertools” module.
    reduce() stores the intermediate result and only returns the final summation value. Whereas, accumulate() returns a list containing the intermediate results. The last number of the list returned is summation value of the list.
    reduce(fun,seq) takes function as 1st and sequence as 2nd argument. In contrast accumulate(seq,fun) takes sequence as 1st argument and function as 2nd argument.
"""
