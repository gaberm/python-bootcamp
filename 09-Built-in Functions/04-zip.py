"""zip() makes an iterator that aggregates elements from each of the iterables.

Returns an iterator of tuples, where the i-th tuple contains the i-th element from each of the argument sequences or iterables. The iterator stops when the shortest input iterable is exhausted. With a single iterable argument, it returns an iterator of 1-tuples. With no arguments, it returns an empty iterator.

zip() is equivalent to:

def zip(*iterables):
    # zip('ABCD', 'xy') --> Ax By
    sentinel = object()
    iterators = [iter(it) for it in iterables]
    while iterators:
        result = []
        for it in iterators:
            elem = next(it, sentinel)
            if elem is sentinel:
                return
            result.append(elem)
        yield tuple(result)
"""

x = [1,2,3]
y = [4,5,6, 7, 8, 9, 10, 11, 12]

# Zip the lists together
# defined by the shortest list..
list(zip(x,y))

d1 = {'a':1,'b':2}
d2 = {'c':4,'d':5}

list(zip(d1,d2))
# This makes sense because simply iterating through the dictionaries will result in just the keys. We would have to call methods to mix keys and values:
list(zip(d1.keys(),d2.values()))
list(zip(d1.values(),d2.keys()))

def switcharoo(d1,d2):
    # lets use zip() to switch the keys and values of the two dictionaries:
    dout = {}

    for d1key,d2val in zip(d1,d2.values()):
        dout[d1key] = d2val

    return dout

switcharoo(d1, d2)

def switchdict(a,b):
    return list(zip(a.keys(),b.values()))

def switchdict2(a,b):
    dout={}
    for aKey, bVal in list(zip(a.keys(),b.values())):
        dout[aKey] = bVal
    for aKey, bVal in list(zip(b.keys(),a.values())):
        dout[aKey] = bVal

    x=  list(zip(a.keys(),b.values())), list(zip(b.keys(),a.values()))
    return dout

list(switchdict(d1, d2))
switchdict2(d1, d2)
