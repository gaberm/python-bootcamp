#!/usr/bin/python3.7
mylist = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for num in mylist:
    # Check for even
    if num % 2 == 0:
        print(num)
    else:
        print( f'Odd Number: {num}')

list_sum = 0
for num in mylist:
    list_sum = list_sum + num
print(list_sum)

mylist = [x**2 for x in range(0,11) if x%2 == 0]
mylist = [x for x in range(0,11) if (x**1/2)%2 == 0]
print(mylist)

celcius = [0,10,20,34.5]
def fahrenheit(x):
    return [(9/5 * x ) + 32]

fahrenheit(20)
results = map (fahrenheit, celcius)
list(results)

print ( [x if x%2==0 else 'ODD' for x in range(0,20)])
print ( [x for x in range(0,20) if x%2 == 0  ] )

print ( (x, y) for x in range(0,11) for y in range(0,11) )
mylist = [x* y for x in [2,4,6] for y in [1,10,100] ]
mylist
=======
# We'll learn how to automate this sort of list in the next lecture
list1 = [1,2,3,4,5,6,7,8,9,10]

for num in list1:
    # print(num)

    # Check for even
    if num % 2 == 0:
        print(num)
for _ in 'Hello World':
    print('Cool!')


# Iterating through Tuples
l = [(2,4),(6,8),(10,12)]

for tup in l:
    print (tup)

# Unpacking
for (t1,t2) in l:
    print( t1)

# Iterating through a dictionary
d = {'k1':1,'k2':2,'k3':3}

for item in d:
    print( item)
for item in d.items():
    print( item)

for key,value in d.items():
    print(key)
    print(value)
>>>>>>> 0c1f181cbea1398786bff742cf8b82e0923c3907
