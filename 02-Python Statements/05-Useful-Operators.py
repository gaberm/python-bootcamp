list(range(0,12,2))

index_count = 0
for letter in 'abcde':
    print( 'At index {} the letter is {}.'.format(index_count, letter))
    print( f'At index {index_count} the letter is {letter}.')
    index_count +=1

index_count = 0
word = 'abcde'

for letter in word:
    # print( 'At index {} the letter is {}.'.format(index_count, letter))
    # print( f'At index {index_count} the letter is {letter}.')
    print( word[index_count])
    index_count +=1


word = 'abcde'

for item in enumerate(word):
    print(item)
    # print(zip(item))
# type(item)

for index, letter in enumerate(word):
    print(f'The index is {index} for letter {letter}.')

list(enumerate('abcde'))

mylist1 = [1,2,3,4,5]
mylist2 = ['a','b','c','d','e']

list(zip(mylist1, mylist2))
for item1, item2 in zip(mylist1,mylist2):
    print('For this tuple, first item was {} and second item was {}'.format(item1,item2))


for a,b in zip(mylist1,mylist2):
    # print('For this tuple, first item was {} and second item was {}'.format(item1,item2))
    print(a)

'x' in [1, 2, 3]
'x' in ['x', 'y', 'z']
'mykey' in {'mykey': 345}

d = {'mykey': 345}
345 in d.values()
345 in d.keys()

from random import shuffle
from random import randint

shuffle(mylist1)
mylist1

randint(0,100)

result = input('Enter a number here:')
# result type is str
input('Press enter to exit!')
