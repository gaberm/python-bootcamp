x = 0

while x < 10:
    print (f'The current value of x is {x}')
    x +=  1
else:
    print('Getting out of the Loop')

""" break: Breaks out of the current closest enclosing loop.
    continue: Goes to the top of the closest enclosing loop.
    pass: Does nothing at all.
"""

x = 0

while x < 10:
    x+=1
    print ('x is currently: ',x)
    print (' x is still less than 10, adding 1 to x')
    if x ==3:
        print ('x=3 Step')
    elif x ==5:
        print( 'x = 5, Breaking!')
        break
    else:
        print ('continuing...')
        continue

x = [1,2,3]
type(x)

for items in x:
    #comment
    pass

mystring = 'sammy'
for letter in mystring:
    if letter == 'a':
        continue # Escaping the current letter and continue with print ( "SMMY")
        pass #continue with no action ( printing the "A")
    print(letter)

x = 0
while x < 5:
    if x == 2:
        break
    print(x)
    x += 1
