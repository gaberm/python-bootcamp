### Use for, split(), and if to create a Statement that will print out words that start with 's':
st = 'Print only the words that start with s in this sentence'
# stlist = st.split()
print( [s for s in st.split() if s[0]=='s'])

### Go through the string below and if the length of a word is even print "even!"
st = 'Print every word in this sentence that has an even number of letters'

print( [s for s in st.split() if len(s)%2 == 0])
### Write a program that prints the integers from 1 to 100. But for multiples of three print "Fizz" instead of the number, and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz". Write a program that prints the integers from 1 to 100. But for multiples of three print "Fizz" instead of the number, and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz".

print( [x if x%3 == 0 else 'No' for x in range(1,101)] )


for x in range(1,101):
    if x%15==0: print(x, 'FizzBuzz')
    elif x%5==0: print(x, 'Buzz')
    elif x%3==0: print(x,'Fizz')


# Use List Comprehension to create a list of the first letters of every word in the string below:
st = 'Create a list of the first letters of every word in this string'

stlist = [x[0] for x in st.split()]
print(stlist)
type(stlist)

### Works in ipython
letters = list(map(lambda x: x, 'human'))
print(letters)

bool=False
def myfunc(bool):
    if bool==True: return 'Hello'
    else:
        return "Goodbye"

myfunc(bool)
