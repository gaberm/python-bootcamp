#/usr/bin/python3.7
player = 'Thomas'
points = 33
# 'Last night, '+player+' scored '+str(points)+' points.'  # concatenation
# f'Last night, {player} scored {points} points.'  # string formatting

# "## Formatting with placeholders"
# "You can use %s to inject strings into your print statements. The modulo `%` is referred to as a " "string formatting operator"
s= 'something'

print("I'm going to inject %s here." %'something diff ')


print("I'm going to inject %s text here, and %s text here." %('some','more'))
x = 'some' ; y = 'some more'
print("I'm going to inject %s text here, and %s text here."%(x,y))

print('He said his name was %s.' %'Fred')
print('He said his name was %r.' %'Fred')
print('Floating point numbers: %2.2f' %(13.144) )
print('First: %s, Second: %5.2f, Third: %r' %('hi!',3.1415,'bye!'))

'String here {} then also {}'.format('something1','something2')
print('This is a string with an {}'.format('insert'))

print('A %s saved is a %s earned.' %('penny','penny'))
print('A {p} saved is a {p} earned.'.format(p='penny'))


### By default, `.format()` aligns text to the left, numbers to the right. You can pass an optional `<`,`^`, or `>` to set a left, center or right alignment:"
print('{0:<8} | {1:^8} | {2:>8}'.format('Left','Center','Right'))
print('{0:<8} | {1:^8} | {2:>8}'.format(11,22,33))

print('This is my ten-character, two-decimal number:%10.2f' %13.579)
print('This is my ten-character, two-decimal number:{0:10.2f}'.format(13.579))

## Formatted String Literals (f-strings)"
#Introduced in Python 3.6, f-strings offer several benefits over the older `.format()` string method described above.
#For one, you can bring outside variables immediately into to the string rather than pass them as arguments through `.format(var)`."
name = 'Fred'
print(f"He said his name is {name}.")

#Pass `!r` to get the string representation:"
print(f"He said his name is {name!r}")

#### Float formatting follows `"result: {value:{width}.{precision}}"`"
#Where with the `.format()` method you might see `{value:10.4f}`, with f-strings this can become `{value:{10}.{6}}`\n"
num = 23.45678
print("My 10 character, four decimal number is:{0:10.4f}".format(num))
print(f"My 10 character, four decimal number is:{num:{8}.{5}}")
#Note that with f-strings, *precision* refers to the total number of digits, not just those following the decimal. This fits more closely with scientific notation and statistical analysis.
# Unfortunately, f-strings do not pad to the right of the decimal, even if precision allows it:"
print("My 10 character, four decimal number is:{0:10.4f}".format(num))
print(f"My 10 character, four decimal number is:{num:8.4f} milli-seconds")
num = 23.456572
print("My 10 character, four decimal number is:{0:10.4f}".format(num))
print(f"My 10 character, four decimal number is:{num:8.4f}")
