#!/usr/bin/python3.7

def monkey_trouble(a_smile, b_smile):
    trb = False
    if a_smile:
        if b_smile:
            trb = True
    else:
        if b_smile == False:
            trb = True

    return trb


x = -2
x.__abs__()

hour = 23

if (hour < 7 or hour > 20):
    print("Trouble")

if (hour < 7 and hour > 20):
    print("Trouble")


def parrot_trouble(talking, hour):
    if (hour < 7 or hour > 20) and (talking == True):
        return True
    else:
        return False


x = 'Heeololeo'

x[0::2]

tag = "<<>>"
word = "Hello"


def make_out_word(out, word):
    # x = len(out) / 2
    return ('{0} {1} {2}').format(out[:2], word, out[-2:])


print(make_out_word(tag, word))


tag[:2]
tag[-2:]


n = 10


def findMultiples(n):
    l = []
    for i in range(1, n):
        if (i % 3 == 0) or (i % 5 == 0):
            l.append(i)
    # print(l)
    return sum(l)


findMultiples(1000)


def Fibonacci(n):
    f0, f1 = 1, 1
    for _ in range(n):
        yield f0
        f0, f1 = f1, f0 + f1


fibs = list(Fibonacci(10))
print (fibs)


def fib(n):
    f0, f1 = 0, 1
    for i in range(n):
        yield f0
        f0, f1 = f1, f0 + f1

        if f1 > 4000000:
            break


fibs = fib(100)
print(list(fibs))


def fib(n):
    f0, f1 = 0, 1
    while f1 < n:
        yield f0
        f0, f1 = f1, f0 + f1


fibs = fib(1000)
print(list(fibs))

# print(fibs(x) for x in zip(fibs))

print([x for x in fibs])

type([x for x in {1, 2, 3}])  # list

"""
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a2 + b2 = c2

For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc. """


def pythag():
    # take 3 ints
    for x in range(1, 1000):
        for y in range(x, 1000):
            for z in range(y, 1000):
                if x ** 2 + y**2 + z**2 == 1000:
                    print("The values are {}, {} and {}".format(x, y, z))
                    break
                    return x, y, z


def pythag():
    # take 3 ints
    for x in range(1, 1000):
        for y in range(x + 1, 1000):
            for z in range(1 + y, 1000):
                if x ** 2 + y**2 + z**2 == 1000:
                    print("The values are {}, {} and {}".format(x, y, z))
                    break
                    return x, y, z


pythag()
6, 8, 30
10, 18, 24


scores = [90, 91, 92, 94, 95, 96, 97, 99, 100]


def add(num1, num2):
    '''returns the sum of the parameters'''
    return num1 + num2


import operator
import timeit
import functools

%timeit
# --> 1000000 loops, best of 3: 799 ns per loop
reduce(add, scores) / len(scores)

# --> 1000000 loops, best of 3: 207 ns per loop
# timeit sum(scores) / len(scores)

# --> 1000000 loops, best of 3: 485 ns per loop
timeit reduce(operator.add, scores) / len(scores)

functools.reduce(add, scores)
functools.reduce(lambda x, y: x + y, [1, 2, 3, 4, 5])
sum(scores)


<< << << < HEAD
import libgen

libgen.main(search='potter')
== == == =
list = [1, 2, [3, 4, 'hellp']]
type(list)

d = {'simple_key': 'hello'}
d['simple_key']

d = {'k1': {'k2': 'hello'}}
d['k1']['k2']

d = {'k1': [1, 2, {'k2': ['this is tricky', {'tough': [1, 2, ['hello']]}]}]}
d['k1'][2]['k2'][1]['tough'][2]

3.0 == 3

l_one = [1, 2, [3, 4]]
l_two = [1, 2, {'k1': 4}]
l_one[2][1]

s = 'hello'
s[::-1]

[0] * 3
[0, 0, 0]

[0, 0] + [0]

list3 = [1, 2, [3, 4, 'Hello']]
list3[2][2]
list3[2][2] = 'goodbye'

2 == 2

1 != 2

1 < 2 < 3

1 < 2 > 3

1 < 2 and 2 < 3
10 == 1 or 2 < 20

not 1 == 1
1 == 1
>>>>>> > 0c1f181cbea1398786bff742cf8b82e0923c3907
