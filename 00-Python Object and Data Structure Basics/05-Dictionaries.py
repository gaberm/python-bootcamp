# Dictionaries#
#We've been learning about *sequences* in Python but now we're going to switch gears and learn about *mappings* in Python.
#If you're familiar with other languages you can think of these Dictionaries as hash tables.
"""This section will serve as a brief introduction to dictionaries and consist of

    "    1.) Constructing a Dictionary
    "    2.) Accessing objects from a dictionary
    "    3.) Nesting Dictionaries
    "    4.) Basic Dictionary Methods

    "So what are mappings? Mappings are a collection of objects that are stored by a *key*, unlike a sequence that stored objects by their relative position.
    This is an important distinction, since mappings won't retain order since they have objects defined by a key.
    "A Python dictionary consists of a key and then an associated value. That value can be almost any Python object."""

## Constructing a Dictionary
my_dict = {'key1':'value1','key2':'value2'}

# Call values by their key
my_dict['key2']

#Its important to note that dictionaries are very flexible in the data types they can hold. For example:"

my_dict = {'key1':123,'key2':[12,23,33],'key3':['item0','item1','item2']}
my_dict['key2'][1]
my_dict['key3'][1]

# Let's call items from the dict
my_dict['key3']
# Can call an index on that
my_dict['key3'][0]

# Can then even call methods on that value
my_dict['key3'][0].upper()

#We can affect the values of a key as well. For instance:"
d = {'k1': 123, 'k2': [0,3,1,2], 'k3': {'insideKey':100}}
d['k2'].sort()
d['k3']['insideKey']


# Subtract 123 from the
my_dict['key1'] = my_dict['key1'] * 123

#Check
my_dict['key1']

#A quick note, Python has a built-in method of doing a self subtraction or addition (or multiplication or division).
#We could have also used += or -= for the above statement. For example:"
my_dict['key1'] -= 123
my_dict['key1']

#We can also create keys by assignment. For instance if we started off with an empty dictionary, we could continually add to it:"
d = {}

# Create a new key through assignment
d['animal'] = 'Dog'
# Can do this with any object
d['answer'] = 42

## Nesting with Dictionaries
#Hopefully you're starting to see how powerful Python is with its flexibility of nesting objects and calling methods on them.
#Let's see a dictionary nested inside a dictionary:"

# Dictionary nested inside a dictionary nested inside a dictionary
d = {'key1':{'nestkey':{'subnestkey':'value'}}}
print(d)
#Wow! That's a quite the inception of dictionaries! Let's see how we can grab that value:"

d['key1']['nestkey']['subnestkey']
# Keep calling the keys
d['key1']['nestkey']['subnestkey']


# Create a typical dictionary
d = {'key1':1,'key2':2,'key3':3}

# Method to return a list of all keys
d.keys()
# Method to grab all values
d.values()

# Method to return tuples of all items  (we'll learn about tuples soon)
d.items()
