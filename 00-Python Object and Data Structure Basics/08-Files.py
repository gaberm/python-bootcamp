import os
""" Files
Python uses file objects to interact with external files on your computer. These file objects can be any sort of file
you have on your computer, whether it be an audio file, a text file, emails, Excel documents, etc.
Note: You will probably need to install certain libraries or modules to interact with those various
file types, but they are easily available. (We will cover downloading modules later on in the course)"""

# Python has a built-in open function that allows us to open and play with basic file types.
# First we will need a file though. We're going to use some IPython magic to create a text file!
#
# ## IPython Writing a File
# #### This function is specific to jupyter notebooks!
#  Alternatively, quickly create a simple .txt file with sublime text editor.
%%writefile myfile.txt
'Hello, this is a quick test file.'
'Two on SECOND'
'Three on THIRD'

## Python Opening a file, Let's being by opening the file test.txt that is located in the same directory as this notebook.
#For now we will work with files located in the same directory as the notebook or .py script you are using,
#It is very easy to get an error on this step:

# To avoid this error,make sure your .txt file is saved in the same location as your notebook, to check your notebook location, use **pwd**:
%pwd
os.chdir('C:\\Users\\Mohammed\\gitlab\\Complete-Python-3-Bootcamp\\00-Python Object and Data Structure Basics\\')

#Alternatively, to grab files from any location on your computer, simply pass in the entire file path. **\n', '\n', "For Windows you need to use double \\ so python doesn't treat the second \\ as an escape character, a file path is in the form:\n", '\n', '    myfile = open("C:\\\\Users\\\\YourUserName\\\\Home\\\\Folder\\\\myfile.txt")\n', '\n', 'For MacOS and Linux you use slashes in the opposite direction:\n', '\n', '    myfile = open("/Users/YouUserName/Folder/myfile.txt")']
# Open the text.txt we made earlier\n', "
my_file = open('myfile.txt')

# We can now read the file\n', 'my_file.read()']
my_file.read()

# But what happens if we try to read it again?\n', '
my_file.read()

#This happens because you can imagine the reading "cursor" is at the end of the file after having read it.
#So there is nothing left to read. We can reset the "cursor" like this:

# Seek to the start of file (index 0)
my_file.seek(0)
# Now read again
contents = my_file.read()

#You can read a file line by line using the readlines method.
# Use caution with large files, since everything will be held in memory.
# We will learn how to iterate over large files later in the course.

my_file.seek(0)
# Readlines returns a list of the lines in the file
# We can loop through the list
# to check, iter chunks in open
contentslst = my_file.readlines()
contentslst[2]
#When you have finished using a file, it is always good practice to close it.
my_file.close()

## Writing to a File, "By default, the `open()` function will only allow us to read the file.
# We need to pass the argument `'w'` to write over the file. For example:"]
# Add a second argument to the function, 'w' which stands for write.
# Passing 'w+' lets us read and write to the files.

my_file = open('test.txt','w+')
my_file.read()

with open ('myfile.txt', mode = 'r') as my_new_file:
    # code goes here..
    contents = my_new_file.read()
    print(contents)
contents

### <strong><font color='red'>Use caution!</font></strong> \n", """
# Opening a file with `'w'` or `'w+'` truncates the original, meaning that anything that was in the original file **is deleted**

# Append to the file
with open ('myfile.txt', mode = 'a') as my_file:
    my_file.write('\nSix on SIXTH')


# Read the file
my_file = open('test.txt','w+')
my_file.write('I created this file')
my_file.seek(0)
my_file.read()

my_file.close()  # always do this when you're done with a file"]
## Appending to a File\n', "Passing the argument `'a'` opens the file and puts the pointer at the end,
#so anything written is appended. Like `'w+'`, `'a+'` lets us read and write to a file.
#If the file does not exist, one will be created."]
my_file = open('test.txt','a+')
my_file.write('\\nThis is text being appended to test.txt')
my_file.write('\\nAnd another line here.')
my_file.seek(0)
print(my_file.read())
my_file.close()

### Appending with `%%writefile`\n', 'We can do the same thing using IPython cell magic:
%%writefile -a test.txt\
'This is text being appended to test.txt\n', 'And another line here.'
#Add a blank space if you want the first line to begin on its own line, as Jupyter won't recognize escape sequences like `\\n`"]

""" File Iteration """
## Iterating through a File\n', '\n', "Lets get a quick preview of a for loop by iterating over a text file.
# First let's make a new text file with some IPython Magic:"]
%%writefile test.txt\n', 'First Line\n', 'Second Line'

#Now we can use a little bit of flow to tell the program to for through every line of the file and do something:
for line in open('test.txt'):
    print(line)
# Don't worry about fully understanding this yet, for loops are coming up soon. But we'll break down what we did above.
# We said that for every line in this text file, go ahead and print that line.
# It's important to note a few things here:
# 1. We could have called the "line" object anything (see example below).
# 2. By not calling `.read()` on the file, the whole text file was not stored in memory.
# 3. Notice the indent on the second line for print. This whitespace is required in Python.

# Pertaining to the first point above
for asdf in open('test.txt'):
    print(asdf)
#["We'll learn a lot more about this later, but up next: Sets and Booleans!"]
