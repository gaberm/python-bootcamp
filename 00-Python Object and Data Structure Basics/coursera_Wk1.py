1024
bin(1024)
hex(1024)

store1= [10.0, 11.0, 12.34, 7.86]
store2 = [9.99, 12.30, 11.5, 8]

cheapest = map(min, store1, store2)
print(cheapest)
print(list(cheapest))

min()

people = ['Dr. Christopher Brooks', 'Dr. Kevyn Collins-Thompson', 'Dr. VG Vinod Vydiswaran', 'Dr. Daniel Romero']

# people[0].split('.')
def split_title_and_name(person):
    return person.split()[0] + ' ' + person.split()[-1]

def split2(person):
    return person.split()[0] + ' ' + person.split()[-1]

list(map(split_title_and_name, people))
list(map(split2, people))

myfunc = lambda a, b, c: a + b * c
myfunc (1, 4 ,5)


people = ['Dr. Christopher Brooks', 'Dr. Kevyn Collins-Thompson', 'Dr. VG Vinod Vydiswaran', 'Dr. Daniel Romero']

def split_title_and_name(person):
    return person.split()[0] + ' ' + person.split()[-1]

#option 1
for person in people:
    print(split_title_and_name(person) == (lambda person: person.split()[0] + ' ' + person.split()[-1]))


splitfn = lambda person: print(person.split()[0]  + ' ' + person.split()[-1])

for i in people:
    print(i)
    print(splitfn(i))

splitfn(people)



map_output = map(lambda x: x*2, [1, 2, 3, 4])
print(map_output) # Output: map object: <map object at 0x04D6BAB0>

list_map_output = list(map_output)
print(list_map_output) # Output: [2, 4, 6, 8]

people = ['Dr. Christopher Brooks', 'Dr. Kevyn Collins-Thompson', 'Dr. VG Vinod Vydiswaran', 'Dr. Daniel Romero']

def split_title_and_name(person):
    return person.split()[0] + ' ' + person.split()[-1]

#option 1

for person in people:
    print(split_title_and_name(person) == (lambda x: x.split()[0] + ' ' + x.split()[-1])(person))


mylist = []
for number in range(0,1000):
    if number %2 == 0:
        mylist.append(number)

mylist

mylist = [number for number in range(0, 1000) if number % 2 == 0]
mylist

def times_tables():
    lst = []
    for i in range(10):
        for j in range (10):
            lst.append(i*j)
    return lst

times_tables() ==  [[i*j for i in range(0,5)] for j in range(0, 5)]
[i*j for i in range(0,10) for j in range(0, 10)]

## Write an initialization line as a single list comprehension which creates a list of all possible user ids. Assume the letters are all lower case.
# Many organizations have user ids which are constrained in some way. Imagine you work at an internet service provider and the user ids are all two letters followed by two numbers (e.g. aa49). Your task at such an organization might be to hold a record on the billing activity for each possible user.
lowercase = 'abcdefghijklmnopqrstuvwxyz'
digits = '0123456789'

correct_answer = [a+b+c+d for a in lowercase for b in lowercase for c in digits for d in digits]

correct_answer[:] # Display first 50 ids


import numpy as np
mylist = [1, 2, 3]
x = np.array(mylist)
x

y = np.array([4,5,6])
z = np.array([x,y])
m = np.array([[7,8,9], [10,11,12]])

n = np.arange(0,30, 2)
n

n.reshape(3,5)

o = np.linspace(0,4, 9)
o.resize(3,3 )
o

np.ones((3,3))
np.zeros((2,2))

np.eye((2))
np.diag(y)

np.array([1,2,3] * 4)
np.repeat([1,2,3], 4)

p=np.ones([2,3], int)
np.vstack([p, 2*p])
np.hstack([p, 2*p])

x + y
x * y
x ** 2
x.dot(y)

np.array([y, y**2])

z.shape
z.T

z.dtype

a =np.array([-4, -2, 1, 3, 5])
a.sum()
a.max()
a.min()
a.mean()
a.std()
a.argmax()
a.argmin()

s = np.arange(13)**2
s
s[0], s[11], s[0:3]
s[1:5]
s[-5::-2]

r= np.arange(36)
r.resize((6,6))
r[2,2]

r[3, 3:6]
r[r>30] #return values regardless of the array shape
r2 = r[:3, :3]
r2[:] = 1
r2


test = np.random.randint(0,10, (4,3))
test

for row in test:
    print(row)

for column in test:
    print(test)

for i in range ( len (test)):
    print(test[i], "..", i )

for i, row in enumerate(test):
    print('row', i, 'is', row)

for i, j in zip(test, test+1):
    # print('row', i, 'is', row)
    print(i, '+', j, '=', i+j)

type(lambda x: x+1) 
