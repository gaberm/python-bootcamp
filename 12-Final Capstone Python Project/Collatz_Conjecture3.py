"""Collatz Conjecture - Start with a number n > 1. Find the number of steps it takes to reach one using the following process: If n is even, divide it by 2. If n is odd, multiply it by 3 and add 1. """


class Collatz():

    #Define class variables.
    count = 0

    def __init__(self,x):
        self.x = x
        # self.count = count

    def oddNum(x):
        x = (3*x) + 1
        # print(x, ' is odd!')
        if x == 1:
            return print(' Collatz achived! and in {} steps'.format(Collatz.count))
        else:
            Collatz.calcColl(x)

    def evenNum(x):
        x = x/2
        # print(x, ' is even! and of type', type(x))

        if x == 1:
            return print(' Collatz achived! and in {} steps'.format(Collatz.count))
        else:
            Collatz.calcColl(x)

    def calcColl(x):

        x = int(x)
        Collatz.count += 1
        if((x&1) == 0):
            Collatz.evenNum(x)
        else:
            Collatz.oddNum(x)

Collatz.calcColl(12)
