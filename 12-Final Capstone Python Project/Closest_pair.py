"""Closest pair problem - The closest pair of points problem or closest pair problem is a problem of computational """
"""geometry: given n points in metric space, find a pair of points with the smallest distance between them."""
## (x1,y1) (x2,y2) (x3,y3) (x4,y4) (x5,y5)

# Distance between two points P(x1,y1) and Q(x2,y2) is given by: d(P, Q) = √ (x2 − x1)2 + (y2 − y1)2 {Distance formula} 2. Distance of a point P(x, y) from the origin is given by d(0,P) = √ x2 + y2. 3.


def distance(a,b): #given a & b are tuples
    # Finding the distance between two points
    # dist = ((x2-x1)**2 + (y2 -y1)**2)**1/2
    # Lambda implement
    #lambda a,b: ( (b[0]-a[0])**2 + (b[1] -a[0])**2 )**(1/2)
    return ( (b[0]-a[0])**2 + (b[1] -a[0])**2 )**(1/2)

def closestPair(point,*args):
    # If given one, find the distance to origin
    # else, find closest
    # dist = ((x2-x1)**2 + (y2 -y1)**2)**1/2
    if not args:
        # print('One arg, distance to Origin ..')
        return print('One arg, distance to Origin:  ', distance(point,(0,0)))
    cdist, cpoint = 99999999,(0,0)
    for points in args:
        # print(points)
        xdist = distance(point, points)
        print('xdist', xdist)
        print('cdist', cdist)
        if xdist < cdist:
            cdist, cpoint = xdist, points
    return ('The closest point is {} from pont {}'.format(cdist,cpoint))


distance((5,5) , (3,3))

closestPair(a)
a,b,c = (5,5) , (3,3) , (7,-1)
closestPair(a,b,c)
closestPair((1,1), (4,4), (10,10), (2,2))



from functools import reduce
reduce(x,l)

t = [4,7,4,98,3,32]
t2 = map(lambda a: a*2,t)

[(a,b) for i in t for j in t2]
list(t2)
reduce(lambda a,b: a if a>b else b, ll)
#Find the maximum of a sequence (This already exists as max())
reduce (lambda a,b: a if (a>b) else b, lst)

x =lambda a,b: ( (b[0]-a[0])**2 + (b[1] -a[0])**2 )**(1/2)
l = [(5,5) , (3,3) , (7,-1), (-1,9), (420, 12)]
reduce (lambda a,b,c: distance(a,b) if (distance(a,b) < distance(a,c)) else distance(a,c), l)

reduce(x,[(3,3),(5,5)] )
reduce(x,[(x, y) for x, y in lst])

lst = [(4,1), (5,1), (3,2), (7,1), (6,0)]
[(x, y) for x, y in lst if distance(x,y) ]
[x+y for x in [10,30,50] for y in [20,40,60]]
[distance((x,y),(x,y)) for x in [10,30,50] for y in [20,40,60]]
[distance(x,y) for x in [(10,1),(30,50)] for y in [(20,25),(40,60)]]
