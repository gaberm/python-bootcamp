"""Multiple child widgets in same parent
A Frame example """

import pythotk as tk

window = tk.Window("Pack Example")

big_frame = tk.Frame(window)
big_frame.pack(fill='both', expand=True)
tk.Label(big_frame, text="Left").pack(side='left', fill='both', expand=True)
tk.Label(big_frame, text="Right").pack(side='right', fill='both', expand=True)

status_bar = tk.Label(window, "This is a status bar")
status_bar.pack(fill='x')

window.geometry(300, 200)
tk.run()
