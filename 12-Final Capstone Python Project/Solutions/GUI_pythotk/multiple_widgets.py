"""Multiple child widgets in same parent
It’s possible to put several different widgets into the same parent window with pack(), like this: """

import pythotk as tk

window = tk.Window("Pack Example")
tk.Label(window, "One").pack(side = 'left')
tk.Label(window, "Two").pack(side = 'left')
tk.Label(window, "Three").pack(side = 'left')
tk.Label(window, "Four").pack(side = 'left')
tk.run()
