
"""Binary to Decimal and Back Converter - Develop a converter to convert a decimal number to binary or a binary number to its decimal equivalent. """

def toInt(n):
    return int(n, 2)

def toBin(n):
    return bin(n)

toBin(261)
toInt('0b1000001011')

add = lambda x,y : x + y
reduce(add, [int(x) * 2 ** y for x, y in zip(list(binstr), range(len(binstr) - 1, -1, -1))])

import functools
from functools import reduce

binstr = '1000001011'
reduce(int.__add__, [int(x) * 2 ** y for x, y in zip(list(binstr), range(len(binstr) - 1, -1, -1))])
