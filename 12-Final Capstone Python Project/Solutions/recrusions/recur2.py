### Problem explantion: https://stackoverflow.com/questions/50754601/implementing-merge-sort-function-in-python-class-errors?newreg=388d6054fadf4b90a448063b3f42901c

class Sort:

    def __init__(self, array):
        self.array = array

    def mergeSort(self):
        """Merge Sort is a Divide and Conquer algorithm. It divides input array in two halves, calls itself for the two halves and then merges the two sorted halves. The merge() function is used for merging two halves """

        print("Called array: ", self.array)

        if len(self.array) > 1:
            print("Splitting ")
            # Finding the middle      [ 5, 1, 4, 2, 8 ]
            mid = len(self.array) //2
            # Splitting the self.array  [5,1]  [4,2,8]
            L = self.array[:mid]
            R = self.array[mid:]
            print("\tLeft Handside ",L)
            print("\tRight Handside ",R)

            # Sorting the splits
            mergeSort(L)
            mergeSort(R)

            # Define counters..
            i, j, k = 0, 0, 0

            while i < len(L) and j < len(R):
                if L[i] < R[j]:
                    self.array[k] = L[i]
                    i += 1
                else:
                    self.array[k] = R[j]
                    j += 1
                k += 1

             # Checking if any element was left
            while i < len(L):
                self.array[k] = L[i]
                i += 1
                k += 1

            while j < len(R):
                self.array[k] = R[j]
                j += 1
                k += 1
        print('Merging ', self.array)

# arr = [12, 11, 13, 5, 6, 7, 1, 2, 4 ,9]
# Sort.mergeSort(arr)

x = Sort([1,6,5,2,10,8,7,4,3,9])
x.mergeSort()
