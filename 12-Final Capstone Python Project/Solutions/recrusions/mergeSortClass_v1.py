class SortClass(object):

    def __init__(self, array):
        self.array = array

    def mergeSort(self):
        if len(self.array) > 1:
            m = len(self.array)//2
            left = self.array[:m]
            right = self.array[m:]

            leftsorter = SortClass(left)
            leftsorter.mergeSort()
            rightsorter = SortClass(right)
            rightsorter.mergeSort()

            i,j,k = 0,0,0
            
            while i < len(left) and j < len(right):
                if left[i] < right[j]:
                    self.array[k] = left[i]
                    i += 1
                else:
                    self.array[k] = right[j]
                    j += 1
                k += 1

            while i < len(left):
                self.array[k] = left[i]
                i += 1
                k += 1

            while j < len(right):
                self.array[k] = right[j]
                j += 1
                k += 1

x = SortClass([3,5,6,2,1,4,10,9,8,7])
x.mergeSort()
x.array
