"""Fast Exponentiation - Ask the user to enter 2 integers a and b and output a^b (i.e. pow(a,b)) in O(lg n) time complexity."""
import math

# a = int(input('Input number pair (a):   '))
# b = int(input('Input number pair (b):   '))
txt = input('Input number pair (a,b):   ')
l = txt.split(',')
a = int(l[0])
b = int(l[1])

def expo(a,b):
    return a**b

def logx(a,b):
    return math.log(a, b)

# Answer
print(expo(a,b))
print(logx(a,b))


""" Given an integer a and positive number b, write a function that computes
a) Time complexity of the function should be O(Log b)
b) Extra Space is O(1)

Examples:
Input: a = 3, b = 5
Output: 243

Input: a = 2, b = 5
Output: 32           """


def power(x, y):
    if(y == 0): return 1

    d = power(x, y>>1 )

    if((y&1) == 0): return d*d
    else: return x*d*d


print( power(2,0) )
print( power(2,3) )
print( power(2,2) )
print( power(2,7) )


""" Bit-Wise operations
x << y      Returns x with the bits shifted to the left by y places (and new bits on the right-hand-side are zeros). This is the same as multiplying x by 2**y.
x >> y      Returns x with the bits shifted to the right by y places. This is the same as //'ing x by 2**y.
x & y       Does a "bitwise and". Each bit of the output is 1 if the corresponding bit of x AND of y is 1, otherwise it's 0.
x | y       Does a "bitwise or". Each bit of the output is 0 if the corresponding bit of x AND of y is 0, otherwise it's 1.
~ x         Returns the complement of x - the number you get by switching each 1 for a 0 and each 0 for a 1. This is the same as -x - 1.
x ^ y       Does a "bitwise exclusive or". Each bit of the output is the same as the corresponding bit in x if that bit in y is 0, and it's the complement of the bit in x if that bit in y is 1. """

# factorial = lambda n: [x for x in range(1,n+1) if not n%x]
flamb = lambda n, m: n<<m
flamb = lambda n, m: n>>m
flamb = lambda n, m: n&m
flamb = lambda n, m: n|m
flamb = lambda n: ~n
flamb = lambda n, m: n^m
flamb(255,1)
