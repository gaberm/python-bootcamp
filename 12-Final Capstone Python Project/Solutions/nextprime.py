### Next Prime Number - Have the program find prime numbers until the user chooses to stop asking for the next one.
nextround = True
factors = lambda n: [x for x in range(1,n+1) if not n%x]
is_prime = lambda n: len(factors(n))==2
# primefactors = lambda n: list(filter(is_prime,factors(n)))

# for i in range (100,1, -1):
#     print(i)

## Upper limit
n = 100

def yielding(n):
    for i in range(n, 2, -1):
        if is_prime(i):
            yield print(i)

x = yielding(n)

while nextround:
    cont = str(input('Should we move to the next prime?  ')).lower()
    try:
        if cont[0].lower() == 'y':
            next(x)
            nextround = True
        else:
            print('Thank you for playing.')
            break
    except:
        break
