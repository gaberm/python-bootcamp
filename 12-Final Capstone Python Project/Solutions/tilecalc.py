""" Find Cost of Tile to Cover W x H Floor - Calculate the total cost of tile it would take to cover a floor plan of width and height, using a cost entered by the user. """

import math

anotheroom = True
def tilecalc(W,H,C):
    #calc are in m2
    space = W*H
    #tile size
    a,b = 60,30
    tilesize= 60*30 / 10000 # in m2
    # round(10.0001+.50001)
    nutiles = math.ceil(space/tilesize)
    cost = nutiles*C
    return cost, space

while anotheroom:
    a = int(input('Please enter the room dimensions (height):  '))
    b = int(input('Please enter the room dimensions (width):   '))
    c = int(input('Please enter the tile cost:                 ') )

    x = tilecalc(a,b,c)
    print(f'The cost would be {x[0]}$ for a space of {x[1]} m2.')
    cont = input('Do you want to calculate another room?  ')
    try:
        if cont[0].lower() == 'y':
            anotheroom = True
            continue
        else:
            print('Thank you.')
            break
    except:
        break

# x = tilecalc(4,5, 10)
# x[1]
