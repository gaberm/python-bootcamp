""" For this challenge, create a bank account class that has two attributes:
    owner
    balance

and two methods:
    deposit
    withdraw

As an added requirement, withdrawals may not exceed the available balance.
Instantiate your class, make several deposits and withdrawals, and test to make sure the account can't be overdrawn."""

class Account:
    def __init__(self, owner, balance):
        self.owner = owner
        self.balance = balance

    def deposit(self,amount):
        self.balance += amount
        print('Deposit accepted!')

    def withdraw(self,amount):
        if amount < self.balance:
            self.balance -= amount
            print('Withdraw accepted!')
        else: print('No sufficient credit..')

    def checkbalance(self):
        print(f'Your current balance is {self.balance}.\nThank you for banking with us.')

# 1. Instantiate the class
acct1 = Account('Jose',100)
# 3. Show the account owner attribute
acct1.balance
acct1.owner

# 2. Print the object
print(acct1)

# 5. Make a series of deposits and withdrawals
acct1.deposit(50)
acct1.withdraw(75)
# Check balance..
acct1.checkbalance()
