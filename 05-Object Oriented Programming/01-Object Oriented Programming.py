class Dog():

    # CLASS OBJECT ATTRIBUTE
    # SAME FOR ANY INSTANCE OF A CLASS
    species = 'mammal'

    def __init__(self, mybreed, name, *spots):
        # Attributes
        # We take in the argument and assign it using self.Attribute_name
        """ We take in the argument and assign it using self.Attribute_name"""
        self.breed = mybreed
        # self.my_attribute = mybreed
        self.name = name
        # Expect boolean T/F
        self.spots = bool(spots)

    # Operations/Actions --> Methods
    def bark(self,number):
        print(f"Woof Woof! My name is {self.name} and I'm {number} years old.")

my_dog = Dog('Huskie', 'Rex')
my_dog = Dog('Lab', 'Sammy', False)

type(my_dog)
print(my_dog.breed)
my_dog.breed
my_dog.name
my_dog.spots
my_dog.species

my_dog.bark(3)
my_dog.bark


class Circle():

    #Class atrrib
    pi = (22/7)

    def __init__(self, radius = 1):
        self.radius = radius
        self.area = (radius**2)*pi

    #Methods
    def get_circum(self):
        # return self.radius * self.pi *2
        return self.radius * Circle.pi *2


my_circle = Circle(30)
my_circle.radius
my_circle.get_circum()
my_circle.area
