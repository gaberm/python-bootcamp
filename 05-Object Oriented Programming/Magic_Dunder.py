class Book():
    """ DocuString: Defining class Book """
    def __init__(self, title, author, pages):
        self.title = title
        self.author=author
        self.pages=pages

    ## Definingg the string repr of Book
    def __str__(self):
        return f"{self.title} by {self.author}"


    ## Defining the length of Book
    def __len__(self):
        return self.pages

    ## Define delete
    def __del__(self):
        print('A book object has been deleted..')


b = Book('Python rocks', 'Jose', 220)
print(b)
str(b)
len(b)

## A way to delete variables / objects
del b
