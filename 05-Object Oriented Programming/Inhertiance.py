class Animal():

    def __init__(self):
        print('Animal CREATED')

    def whoami(self):
        print('I am an animal')

    def eat(self):
        print("I'm eating")

myanimal = Animal()

myanimal.eat()
myanimal.whoami()


class Dog(Animal):   #Inherting Animal Class, making it a Derived class..

    def __init__(self):
        Animal.__init__(self)
        print('Dog CREATED')

    def whoami(self):
        print("I'm a dog.")

    def park(self):
        print('Woof WOOF!')


mydog = Dog()
mydog.whoami()
mydog.eat()
mydog.park()
