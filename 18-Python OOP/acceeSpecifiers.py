# Public => memberName
# Protected => _memberName
# Provate => __memberName

"""Inheritance

Inheritance: is the attributes and methods of a base class are being inherited by a derived class.
- derived class will have access to the attributes and methods of the base class and also it can have its own attributes and methods.

* single inheritance is when a derived class inherits from just one base class and the syntax to define a single inheritance is to have your name of the base class within parentheses at the time of creation of your derived class.

* multiple inheritance is when a derived class inherits from multiple base classes. The derived class will have access to the attributes and methods of all these base classes.
The syntax to define multiple inheritance is to separate the base classes with comma.

* multi-level inheritance
The syntax to define multiple inheritance is to separate the base classes with comma and then you learnt about multi-level inheritance.
Now this is when a series of inheritance happens. You have a derived class which inherits from a base class and this derived class becomes a base class of another derived class.

So the derived class at the bottom most level will have access to the attributes and methods to its family of base classes

** naming conventions used for class members
- variable name for your public member.
- _variableName for your protected member which means to say this member should be accessed only within your class and your derived class.
- __variableName private members by using double underscores which means to say that this particular member should not be accessed outside this class.

*** name mangling which is when you declare an attribute with a double underscore,
it is being prepended with a single underscore and the class name.
object._class__variablename
"""


class Car:
    numberOfWheels = 4
    _color = 'Black'
    __yearOfManuf = 2017  # name mangling =>> _Car.__yearOfManuf


class BMW(Car):
    def __init__(self):
        print(f'Protected attribute color: {self._color}')


car = Car()
bmw = BMW()
print(f'Public attribute numberOfWheels: {car.numberOfWheels}')
# print(f'Private attribute yearOfManufacture: {car.__yearOfManuf}')
print( f'Private attribute yearOfManufacture, name mangled: {car._Car__yearOfManuf}')
