"""Case I: Method will not override this method in both class B and class C
Case II: Method will be overriden in class B but not in class C.
Case III: Method will be overriden in class C but not in class B.
and Case IV: Method will be overriden in both classes B and  class C"""
## MRO, method resolation order
class A:
    def method(self):
        print('This method belongs to class A')
    pass

class B(A):
    def method(self):
        print('This method belongs to class B')
    pass


class C(A):
    def method(self):
        print('This method belongs to class C')
    pass


class D(B,C):
    pass

d = D()
d.method()
