class Square():

    def __init__(self, side):
        self.side = side

    def __add__(SquareA, SquareB):
        return ((SquareA.side*4)+(SquareB.side*4))

    def area(self):
        print(f'Area of square: {(self.side)**2}')

class Rectangle:
    def __init__(self, width, length):
        self.width = width
        self.length = length

    def area(self):
        print(f'Area of rectangle: {(self.width * self.length )}')

square = Square(4)
rectangle = Rectangle(4,6)
square.area()
rectangle.area()
# SqA = Square(4)
# SqB = Square(8)
# print(SqA+SqB)
