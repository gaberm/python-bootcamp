"""Instance method are methods which require an object of its class to be created before it can be called. Static methods are the methods in Java that can be called without creating an object of class. Static method is declared with static keyword. Instance method is not with static keyword."""

class Employee:

    def __init__(self, name):
        self.name = name

    def displayEmployeeDetails(self):
        print(self.name )

    @staticmethod               # Can also use the staticmethod()
    def welcomeMessage():
        print('Welcome to our org..')

employee = Employee('Mark')
employeeTwo = Employee('Ben')
employee.displayEmployeeDetails()
employeeTwo.displayEmployeeDetails()
employee.welcomeMessage()
