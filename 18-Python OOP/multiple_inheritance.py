class OperatingSystem:
    multitasking = True
    name = 'Mac OS'

class Apple:

    manufacturer = "Apple Inc."
    contactWebsite = "www.apple.com/contactus"
    website = "www.apple.com"
    name = 'Apple'

    def contactDetails(self):
        print('To contact us, log on to', self.contactWebsite)


class MacBook(OperatingSystem, Apple):

    def __init__(self):
        self.yearOfManufacture = 2017
        if self.multitasking is True:
            print(f'This is a multitasking system, visit {self.website} for more details.')

    def manufactureDetails(self):
        print(f'This MacBook was manufactured in the year {self.yearOfManufacture} by {self.manufacturer}')
        print(f'Name: {self.name}')

macbook = MacBook()
macbook.manufactureDetails()
macbook.contactDetails()
