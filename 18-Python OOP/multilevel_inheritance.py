class MusicalInstruments:
    numberOfMajorKeys = 12

class StringInstruments(MusicalInstruments):
    typeOfWoord = "Tonewood"

class Guitar(StringInstruments):

    def __init__(self):
        self.numberOfStrings = 6
        print(f'This guitar consists of {self.numberOfStrings} strings, It is made of {self.typeOfWoord} and it can play {self.numberOfMajorKeys} keys.')

Stratavarious = Guitar()
