def square(num):
    return num**2

mynums = [1,2,3,4,5]

for item in map(square,mynums):
    print(item)

list(map(square,mynums))


"""filter function

The filter function returns an iterator yielding those items of iterable for which function(item) is true. Meaning you need to filter by a function that returns either True or False. Then passing that into filter (along with your iterable) and you will get back only the results that would return True when passed to the function.
"""

nums = [0,1,2,3,4,5,6,7,8,9,10]

def check_even(num):
    return num % 2 == 0

filter(check_even,nums)
map(check_even, nums)

list(filter(check_even,nums))
list(map(check_even, nums))


"""lambda expression
Function objects returned by running lambda expressions work exactly the same as those created and assigned by defs. There is key difference that makes lambda useful in specialized roles: lambda's body is a single expression, not a block of statements. """

sq= lambda num: num ** 2
sq(2)

list(map(lambda num: num ** 2, mynums))
list(filter(lambda n: n % 2 == 0,nums))


string = 'hello world!'
s=lambda s: s[::-1]
print(s(string))
