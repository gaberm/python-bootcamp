def myfunc(*args):
    for value in args:
        print(value)

x = [1,2,3,4]
y = (1,2,3,4)
myfunc(40,60,50,10)
myfunc(x)
myfunc(y,x)
myfunc(list(x))

def myfunc(**kwargs):
    print(kwargs)
    if 'fruit' in kwargs:
        # print(f'My fruit of choice is {kwargs['fruit']}')
        print('My fruit of choice is {}'.format(kwargs['fruit']))
    else:
        print("I didn't find any here.")

myfunc(fruit='apple', veggie='lettuce')


def myfunc(*args, **kwargs):
    print(args)
    print(kwargs)
    print('I would like {} {}'.format(args[0], kwargs['food']))

myfunc(10,20,30, fruit='orange', food='eggs', animal='dog')

def myfunc(*args):
    mylist = []
    mylist.append([x for x in args if x%2 ==0])
    return mylist

x = myfunc(-2, 5,6,7,8,9)
print(list(x))

y = (5,6,7,8,9)
print([x for x in y if x%2 == 0])

def myfunc(string):
    # DocuString
    # input
    # outputs
    retstr=''
    for i in range(len(string)):
        if i%2==0: retstr+=string[i].upper()
        else: retstr+=string[i].lower()

    return retstr
x = myfunc('hello')
print(x)

txt='H'
txt += 'i'

list1 = ['1','2','3','4']

s = "-"
s = s.join(list1)
s

s = ' '
s= s.join(list1)
s

# Joining with empty separator
list1 = ['g','e','e','k', 's']
print("".join(list1))
