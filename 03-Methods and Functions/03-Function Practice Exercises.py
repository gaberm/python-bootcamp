# Function Practice Exercises\n",
## Problems are arranged in increasing difficulty

"""ESSER OF TWO EVENS: Write a function that returns the lesser of two given numbers if both numbers are even, but returns the greater if one or both numbers are odd

lesser_of_two_evens(2,4) --> 2
lesser_of_two_evens(2,5) --> 5"""
def lesser(a,b):
    if (a%2==0) and (b%2==0):
        return min(a,b)
    else:
        return max(a,b)

print( lesser(4, 8) )


"""ANIMAL CRACKERS: Write a function takes a two-word string and returns True if both words begin with same letter

animal_crackers('Levelheaded Llama') --> True
animal_crackers('Crazy Kangaroo') --> False"""

def cracker(a):
    a = a.split()
    if a[0][0] == a[1][0]:
        return True
    else: return False

bool = cracker("Lily Bama")



"""MAKES TWENTY: Given two integers, return True if the sum of the integers is 20 or if one of the integers is 20. If not, return False

makes_twenty(20,10) --> True
makes_twenty(12,8) --> True
makes_twenty(2,3) --> False """

def Make20(a,b):
    if (a+b==20) or (a==20) or (b==20):
        return True
    else: return False

Make20(5,15)



"""OLD MACDONALD: Write a function that capitalizes the first and fourth letters of a name
old_macdonald('macdonald') --> MacDonald
Note: 'macdonald'.capitalize() returns 'Macdonald'"""

a = 'hello'
a[1:].capitalize()
b = a[0].capitalize()+a[1:3]+a[3].capitalize()+a[4:]
b

def macdo(a):
    try:
        b = a[0].capitalize()+a[1:3]+a[3].capitalize()+a[4:]
        return b
    except:
        print('Text should be 4 letters or more.')

macdo('pce')

"""
MASTER YODA: Given a sentence, return a sentence with the words reversed

master_yoda('I am home') --> 'home am I'
master_yoda('We are ready') --> 'ready are We'
"""

def yoda(text):
    text=text.split()
    text.reverse()
    # text.count(text) ## Count occurances
    return text

def yoda2(text):
    text=text.split()
    # print(text)
    return text[::-1]


yoda('we are ready to eat!') == yoda2('we are ready to eat!')

"""ALMOST THERE: Given an integer n, return True if n is within 10 of either 100 or 200

almost_there(90) --> True
almost_there(104) --> True
almost_there(150) --> False
almost_there(209) --> True """

def almost(a):
    if (abs(100 - abs(a)) <= 10) or (abs(200 - abs(a)) <= 10):
        return True
    else: return False


almost(191)

""" LEVEL 2 PROBLEMS
FIND 33: Given a list of ints, return True if the array contains a 3 next to a 3 somewhere.

has_33([1, 3, 3]) → True
has_33([1, 3, 1, 3]) → False
has_33([3, 1, 3]) → False        """

def has33(mlist):
    for i in range(len(mlist)-1):
        if (mlist[i]==3) and (mlist[i+1]==3):
            return True
            break
    else: return False

def has33(mlist):
    for i in range(len(mlist)-1):
        if (mlist[i]==3) and (mlist[i+1]==3):
            return True
    return False

def has33(mlist):
    for i in range(len(mlist)-1):
        if [mlist[i] ,mlist[i+1]]==[3,3]:
            return True
    return False

has33([1,2,3,3,4,3,5,3,2])


""" PAPER DOLL: Given a string, return a string where for every character in the original there are three characters """
import timeit  # Timing module
def paper_doll(string):

    # input: paper_doll('Hello') --> 'HHHeeellllllooo'
    # input: paper_doll('Mississippi') --> 'MMMiiissssssiiippppppiii'
    retext=''
    for letter in string:
        # retext.join(letter*3)
        retext+= letter*3
    return retext

def paper_doll2(string):
    # input: paper_doll('Hello') --> 'HHHeeellllllooo'
    # input: paper_doll('Mississippi') --> 'MMMiiissssssiiippppppiii'
    l = []
    for letter in string:
        l.append(letter*3)
        retext = ''.join(l)
    return retext

a = paper_doll('hello')
b = paper_doll2('hello')

# timeit statement in mSecs
import timeit
min( timeit.repeat(stmt="paper_doll('Mississippi')",  setup = "from __main__ import paper_doll", number = 10000, repeat=9))*1000
min( timeit.repeat(stmt="paper_doll2('Mississippi')",  setup = "from __main__ import paper_doll2", number = 10000, repeat=9))*1000


### SUMMER OF '69: Return the sum of the numbers in the array, except ignore sections of numbers starting with a 6 and extending to the next 9 (every 6 will be followed by at least one 9). Return 0 for no numbers.
#
# summer_69([1, 3, 5]) --> 9
# summer_69([4, 5, 6, 7, 8, 9]) --> 9
# summer_69([2, 1, 6, 9, 11]) --> 14

def summer_69(arr):
    total = 0
    add = True
    for num in arr:
        while add:
            if num != 6:
                total += num
                break
            else:
                add = False
        while not add:
            if num != 9:
                break
            else:
                add = True
                break
    return total




""" SPY GAME: Write a function that takes in a list of integers and returns True if it contains 007 in order """
 #
 # spy_game([1,2,4,0,0,7,5]) --> True
 # spy_game([1,0,2,4,0,5,7]) --> True
 # spy_game([1,7,2,0,4,5,0]) --> False

def spy_game(nums):

    code = [0,0,7,'x']

    for num in nums:
        if num == code[0]:
            code.pop(0)   # code.remove(num) also works

    return len(code) == 1

spy_game([1,1,12,0,0,1,7,3,4])



""" COUNT PRIMES: Write a function that returns the number of prime numbers that exist up to and including a given number """
def count_primes2(num):
    primes = [2]
    x = 3
    if num < 2:
        return 0
    while x <= num:
        for y in primes:  # use the primes list!
            if x%y == 0:
                x += 2 # Prime numbers are not even Numbers!!!
                break
        else:
            primes.append(x)
            x += 2
    print(primes)
    return len(primes)

count_primes2(25)
