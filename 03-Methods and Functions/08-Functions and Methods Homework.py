""" Write a function that computes the volume of a sphere given its radius. """
def vol(rad):
    return ((4*22)/(3*7) * rad**3)
vol(2)

""" Write a function that checks whether a number is in a given range (inclusive of high and low) """
def ran_check(num,low,high):
    return (num >= low) and (num <= high)

ran_check(10,2,11)

"""Write a Python function that accepts a string and calculates the number of upper case letters and lower case letters. """
# HINT: Two string methods that might prove useful: .isupper() and .islower()

lower = 0
upper = 0
for letter in 'Hello Mr. Rogers, how are you this fine Tuesday?':
    if letter.isupper(): upper +=1
    elif letter.islower(): lower+=1

print(upper,lower)
print(len('Hello Mr. Rogers, how are you this fine Tuesday?'))


"""Write a Python function that takes a list and returns a new list with unique elements of the first list.

Sample List : [1,1,1,1,2,2,3,3,3,3,4,5]
Unique List : [1, 2, 3, 4, 5]"""

l = [1,1,1,1,2,2,3,3,3,3,4,5]
l2 = list(set(l))
type(l2)


"""Write a Python function to multiply all the numbers in a list."""
def multiply(nums):
    x = 1
    # x = (x*y for y in nums)
    for item in nums:
        x *= item
    # reduce(x*y, nums)
    # print(x)
    return x

multiply([1,2,3,-4])


## Write a Python program to solve (x + y) * (x + y).
def PyBasic(x,y):
    result = x**2 + y**2 + 2*x*y
    return result

PyBasic(4,3)


"""Write a Python function that checks whether a passed in string is palindrome or not.

Note: A palindrome is word, phrase, or sequence that reads the same backward as forward, e.g., madam or nurses run"""

def palindrome(s):
    return s[::]==s[::-1]

palindrome('helleh')


"""Write a Python function to check whether a string is pangram or not.

Note : Pangrams are words or sentences containing every letter of the alphabet at least once.
For example : "The quick brown fox jumps over the lazy dog" """

# string.ascii_lowercase

def ispangram(str1):
    import string
    alphabet=string.ascii_lowercase
    def func(letter):
        print(letter in alphabet for letter in str1)

    map(func, str1)

ispangram('The quick brown fox jumps over the lazy dog')




import string

def ispangram(str1, alphabet=string.ascii_lowercase):
    alphaset = set(alphabet)
    return alphaset <= set(str1.lower())

ispangram("The quick brown fox jumps over the lazy dog")

set(("The quick brown fox jumps over the lazy dog").lower())
