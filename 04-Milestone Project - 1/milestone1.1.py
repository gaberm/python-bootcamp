"""Not Working"""
import numpy as np
import random
from IPython.display import clear_output
from tabulate import tabulate # Used for Charts graphical representation

def getPlayers():
    while True:
        player1 = input('Please choose a character X or O or q to quit:').upper()

        if (player1 != "X") and (player1 != "O") and (player1 != 'Q'):
            print("Sorry I didn't understand that!")
            continue
        if player1 == 'Q':
            print('Player1 has quit the game','')
            break
        else:
            #arg was success
            break

    if (player1 == "X"):
        player2 ='O'
    else:
        player2 ='X'

    player2_cont = input(f'Player 2 is now {player2}, enter y to continue or n to quit:')
    player2_cont= player2_cont.capitalize()

    if player2_cont == 'N':
        return ('Player2 has quit the game', 'q')

    print(f'Thank you, Now player2 is {player2} and we are ready to start' )
    # return player1, player2
    # return {'Py1': player1, 'Py2': player2}
    return (player1, player2)

def displayBoard():
    clear_output()
    print(theboard[7]+'|'+theboard[8]+'|'+theboard[9])
    print('-|-|-')
    print(theboard[4]+'|'+theboard[5]+'|'+theboard[6])
    print('-|-|-')
    print(theboard[1]+'|'+theboard[2]+'|'+theboard[3])
    boardCheck()

def boardCheck():

    winflag= False
    for i in range(1,9,3):
        if (theboard[i]==theboard[i+1]==theboard[i+2]=='O') or  (theboard[i]==theboard[i+1]==theboard[i+2]=='X'):
            # print('Game over, player won!')
            winflag = True

        if (theboard[1]==theboard[5]==theboard[9]=='O') or (theboard[3]==theboard[5]==theboard[7]=='X') or (theboard[1]==theboard[5]==theboard[9]=='X') or (theboard[3]==theboard[5]==theboard[7]=='O'):
            # print('Game over, player won!')
            winflag = True

    if full_board_check() == False:
        return ('Tie Game!!')

def placeMarker(marker, position):
    theboard[int(position)] = marker

def choose_first():
    flip = random.randint(0,1)
    if flip == 0:
        return 'player 1'
    else : return 'player 1'

def full_board_check():
    test = theboard
    test.sort()
    if test[0] == ' ':
        return True
    else: return False

def player_choice():
    position = 0
    while position not in range(1,10) :#or theboard[position]==' ':
        position = int(input("choose a position (1-9).."))
        # print(position)
    return position

def replay():
    choice = input("Play again? Enter Yes or No..")
    return choice == 'Yes'




# While loop to play
print( " Welcome to Tic Tac Toe..")

while True:
    #Play the Game

    ## Set up
    ## Setting the board
    # tictac=np.zeros((3,3), dtype='str') #Intialize the board
    theboard = ['#']+[' ']*9
    # placeMarker("X", 7)

    # l{'Py1', 'Py2'} = getPlayers()
    player1_marker, player2_marker = getPlayers()

    turn = choose_first()
    print(turn+ " will go first")

    ## Game Play

    ## Player One
    if turn == 'Player 1':
        displayBoard()
        position = player_choice()
        marker = player1_marker
        placeMarker(player1_marker, position)
        boardCheck()
        # if winflag:
        #     print('Player 2 won!')
        turn = 'Player 2'
    ## Player Two
    else:
        displayBoard()
        position = player_choice()
        marker = player2_marker
        placeMarker(player1_marker, position)
        boardCheck()
        # if winflag:
        #     print('Player 2 won!')
    if not replay():
        break
# Break out
