import pandas as pd
import os

x = os.listdir()
print([x for x in os.listdir()])


df1 = pd.DataFrame()
df1
df1 = pd.DataFrame([2,4,6],[10,20,30])
df1
# Defining a DF
df1 = pd.DataFrame([[2,4,6],[10,20,30]])
df1
# Defining a DF with Index
df1 = pd.DataFrame([[2,4,6],[10,20,30]],[3,6])
df1

df1 = pd.DataFrame([[2,4,6],[10,20,30]])
# Defining a DF with Column labels
df1 = pd.DataFrame([[2,4,6],[10,20,30]],columns = ['Price', 'Age', 'Value'])

# A frame of Dicts
df2 = pd.DataFrame([{'Name': 'John'}])
df2
df2 = pd.DataFrame([{'Name': 'John'}])
df2.append([{'Name': 'Berry'}])
df2.append([{'Age': 21}], sort = False)

df1.mean()
df1.mean().mean()
df1['Age'].mean()

df1.columns=('Price', 'Age', "Value")

# Loading DF
df3 = pd.read_csv('supermarkets.csv')
df4 = pd.read_json('supermarkets.json')
df5 = pd.read_csv('supermarkets-commas.txt')
df6 = pd.read_csv('supermarkets-semi-colons.txt', sep=';')
df7 = pd.read_json('supermarkets.json')
df8 = pd.read_excel('supermarkets.xlsx')

print(df1,df3,df4,df5,df6,df7,df8)

from bs4 import BeautifulSoup
import requests
r = requests.get("http://en.wikipedia.org/wiki/Eagle")

print(r.content)

df7['Address'][0]
df7.set_index('ID')
help(df7.set_index)

df7.loc[1:3, ['Country', 'ID']]

df7.loc[2:][ 'Address']
df7.iloc[0:3][0:]
df7.loc[1:4]['Address']
df7.iloc[1:4]['Address']
df7.iloc[3,1:4]

# Add and populate a new column
len(df7.index)
df7['Continent'] = df7.shape[0]* ['North America']

## modifying a new column
df7['Continent'] = df7['Country'] + ", " + ['North America']
# Adding a new row
df7_t = df7.T
print(df7_t)
df7_t[6] = ['My_address','My City', 'My Country', 10,7, "Mo's", 'NJ', "USA, North America"]
df7_t.drop('My_address', axis = 1, inplace = True)
df7= df7_t.T
