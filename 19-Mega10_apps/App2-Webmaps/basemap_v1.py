import os
import folium
import pandas as pd

# pwd
os.chdir("D:\\home\\GitLibrary\\Python_bootcamp\\19-Mega10_apps\\App2-Webmaps\\")
cord =  pd.read_csv("Volcanoes.txt")
lat = list(cord['LAT'])
lon = list(cord['LON'])

cord['TICK'] = cord['NAME'] + " with Elev " + cord['ELEV'].astype(str)
# cord['TICK'] = cord[['NAME', 'ELEV']].apply(lambda x: ''.join(str(x)), axis=1)

vname = list(cord['TICK'])

map = folium.Map(location=[38.2, -99.1], zoom_start=5, tiles="Mapbox Bright")
fg = folium.FeatureGroup(name="My Map")

for lt, ln, vn in zip(lat,lon,vname):
    # fg.add_child(folium.Marker(location=[38.2, -99.1], popup="Hi, I'm a Marker", icon = folium.map.Icon(color='green')))
    fg.add_child(folium.Marker(location=[lt,ln], popup=vn + ' m', icon = folium.map.Icon(color='green')))
    ## popup= folium.popup(str(el), parse_html=True)
    # print(lat, lon)

map.add_child(fg)

map.save("Map1.html")
