# https://gist.github.com/Zearin/2f40b7b9cfc51132851a
"""Best practices: decorators

Decorators were introduced in Python 2.4, so be sure your code will be run on >= 2.4.
Decorators slow down the function call. Keep that in mind.
You cannot un-decorate a function. (There are hacks to create decorators that can be removed, but nobody uses them.) So once a function is decorated, it’s decorated for all the code.
Decorators wrap functions, which can make them hard to debug. (This gets better from Python >= 2.5; see below.)

The functools module was introduced in Python 2.5. It includes the function functools.wraps(), which copies the name, module, and docstring of the decorated function to its wrapper. """

import functools
# import time
# print( time.clock())
# print(time.perf_counter())
# print(time.process_time())
# time.perf_counter or time.process_time

# def decorator(func):
#     # We say that `wrapper`, is wrapping `func` and the magic begins
#     @functools.wraps(func)
#     def wrapper():
#         return func()
#     return wrapper
#
# @decorator
# def decorated():
#     code goes here!
#
# decorated()
# print( decorated.__name__)

""" Decorator examples ..."""
import functools

def benchmark(func):
    """
    A decorator that prints the time a function takes to execute.
    """
    import time

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        t1 = time.perf_counter()
        res = func(*args, **kwargs)
        t = (time.perf_counter()-t1)* 1000
        # print( func.__name__, t )
        print('\nelapsed time: {0:.3f} msecs when using {1} func.'.format(t, func.__name__))
        return res
    return wrapper
# help('FORMATTING')
# '{0:{fill}{align}16}'.format(text, fill=align, align=align)

def logging(func):
    """
    A decorator that logs the activity of the script.
    (it actually just prints it, but it could be logging!)
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        print( func.__name__, args, kwargs)
        return res
    return wrapper


def counter(func):
    """
    A decorator that counts and prints the number of times a function has been executed
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        wrapper.count = wrapper.count + 1
        res = func(*args, **kwargs)
        print( '{0} has been used: {1}x'.format(func.__name__, wrapper.count))
        return res
    wrapper.count = 0
    return wrapper

# @counter
# @benchmark
@logging
def reverse_string(string):
    return str(reversed(string))

print( reverse_string('Able was I ere I saw Elba'))
print( reverse_string('A man, a plan, a canoe, pasta, heros, rajahs, a coloratura, maps, snipe, percale, macaroni, a gag, a banana bag, a tan, a tag, a banana bag again (or a camel), a crepe, pins, Spam, a rut, a Rolo, cash, a jar, sore hats, a peon, a canal: Panama!'))


@counter
@benchmark
@logging
def get_random_futurama_quote():
    from urllib import request
    result = request.urlopen('http://www.parseerror.com/pizza/futurama/').read()
    try:
        value = result.split('<br><b><hr><br>')[1].split('<br><br><hr>')[0]
        # <table>\n </div>\n</div>\n<div class="qt">\n ...at least here you\'ll be treated with dignity. Now strip naked and get on\nthe probulator. <div class="who">\n <table>
        return value.strip()
    except:
        return 'No, I’m ... doesn’t!'


print( get_random_futurama_quote())
print( get_random_futurama_quote())

##
##
## Exercise, random quote from Futurma
## http://www.parseerror.com/pizza/futurama/

from urllib import request
from bs4 import BeautifulSoup
wread = request.urlopen('http://www.parseerror.com/pizza/futurama/').read()
print( wread.split( b"<table>\\n </div>\\n</div>\\n<div class=\"qt\">" ) )

soup = BeautifulSoup(wread, 'html.parser')
soup.find_all("div", class_="qt")[0]

for each_div in soup.findAll('div',{'class':'qt'}):
    print (each_div)

print(soup.strings)

for string in soup.strings:
    print(repr(string).split("'\\n'"))

soup.prettify(formatter="minimal")
soup.a
soup.title
soup.table
soup.parent
soup.findAll('table',{'td class':"n"})

for parent in soup.a.parents:
    if parent is None:
        print(parent)
    else:
        print(parent.name)

soup.nextSibling

a = soup.findAll('div',{'class':'qt'})

import pandas as pd
import tabulate

df = pd.read_html(wread)
print(df)
