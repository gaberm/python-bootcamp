# https://gist.github.com/Zearin/2f40b7b9cfc51132851a
"""Passing arguments to the decorator:
Great, now what would you say about passing arguments to the decorator itself?
This can get somewhat twisted, since a decorator must accept a function as an argument. Therefore, you cannot pass the decorated function’s arguments directly to the decorator.
Before rushing to the solution, let’s write a little reminder:"""

# Decorators are ORDINARY functions
def my_decorator(func):
    print( 'I am an ordinary function')
    def wrapper():
        print( 'I am function returned by the decorator')
        func()
    return wrapper

# Therefore, you can call it without any '@'

def lazy_function():
    print( 'zzzzzzzz')

lazy_function()

decorated_function = my_decorator(lazy_function)
#outputs: I am an ordinary function

# It outputs 'I am an ordinary function', because that’s just what you do:
# calling a function. Nothing magic.

@my_decorator
def lazy_function():
    print( 'zzzzzzzz')

#outputs: I am an ordinary function

"""It’s exactly the same: my_decorator is called. So when you @my_decorator, you are telling Python to call the function labelled by the variable “my_decorator”.
This is important! The label you give can point directly to the decorator—or not."""

def decorator_maker():

    print( 'I make decorators! I am executed only once: '+\
        ' when you make me create a decorator.')

    def my_decorator(func):

        print( 'I am a decorator! I am executed only when you decorate a function.')

        def wrapped():
            print ('I am the wrapper around the decorated function. '
                  'I am called when you call the decorated function. '
                  'As the wrapper, I return the RESULT of the decorated function.')
            return func()

        print( 'As the decorator, I return the wrapped function.')

        return wrapped

    print( 'As a decorator maker, I return a decorator')
    return my_decorator

# Let’s create a decorator. It’s just a new function after all.
new_decorator = decorator_maker()
#outputs:
#I make decorators! I am executed only once: when you make me create a decorator.
#As a decorator maker, I return a decorator

# Then we decorate the function

def decorated_function():
    print( 'I am the decorated function.')

decorated_function = new_decorator(decorated_function)
#outputs:
#I am a decorator! I am executed only when you decorate a function.
#As the decorator, I return the wrapped function

# Let’s call the function:
decorated_function()
#outputs:
#I am the wrapper around the decorated function. I am called when you call the decorated function.
#As the wrapper, I return the RESULT of the decorated function.
#I am the decorated function.

"""A Slimmed down version ... """

def decorated_function():
    print( 'I am the decorated function.')
decorated_function = decorator_maker()(decorated_function)
#outputs:
#I make decorators! I am executed only once: when you make me create a decorator.
#As a decorator maker, I return a decorator
#I am a decorator! I am executed only when you decorate a function.
#As the decorator, I return the wrapped function.

# Finally:
decorated_function()
#outputs:
#I am the wrapper around the decorated function. I am called when you call the decorated function.
#As the wrapper, I return the RESULT of the decorated function.
#I am the decorated function.

@decorator_maker()
def decorated_function():
    print( 'I am the decorated function.')


"""Hey, did you see that? We used a function call with the @ syntax! :-)
So, back to decorators with arguments. If we can use functions to generate the decorator on the fly, we can pass arguments to that function, right?"""

def decorator_maker_with_arguments(decorator_arg1, decorator_arg2):

    print( 'I make decorators! And I accept arguments:', decorator_arg1, decorator_arg2)

    def my_decorator(func):
        # The ability to pass arguments here is a gift from closures.
        # If you are not comfortable with closures, you can assume it’s ok,
        # or read: http://stackoverflow.com/questions/13857/can-you-explain-closures-as-they-relate-to-python
        print( 'I am the decorator. Somehow you passed me arguments:', decorator_arg1, decorator_arg2)

        # Don’t confuse decorator arguments and function arguments!
        def wrapped(function_arg1, function_arg2):
            print( 'I am the wrapper around the decorated function.\n'
                  'I can access all the variables\n'
                  '\t- from the decorator: {0} {1}\n'
                  '\t- from the function call: {2} {3}\n'
                  'Then I can pass them to the decorated function'
                  .format(decorator_arg1, decorator_arg2,
                          function_arg1, function_arg2))
            return func(function_arg1, function_arg2)

        return wrapped

    return my_decorator

@decorator_maker_with_arguments('Leonard', 'Sheldon')
def decorated_function_with_arguments(function_arg1, function_arg2):
    print ('I am the decorated function and only knows about my arguments: {0}'
           ' {1}'.format(function_arg1, function_arg2))

decorated_function_with_arguments('Rajesh', 'Howard')

# Here it is: a decorator with arguments. Arguments can be set as variable:
c1 = 'Penny'
c2 = 'Leslie'

@decorator_maker_with_arguments('Leonard', c1)
def decorated_function_with_arguments(function_arg1, function_arg2):
    print ('I am the decorated function and only knows about my arguments:'
           ' {0} {1}'.format(function_arg1, function_arg2))

decorated_function_with_arguments(c2, 'Howard')
