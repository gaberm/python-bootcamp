""" Pyhton Applet to block some sites during working hours"""
 # Windows Execution Premissions # https://stackoverflow.com/questions/36434764/permissionerror-errno-13-permission-denied
# Windows Service - running Applet as a Service #
# https://stackoverflow.com/questions/32404/how-do-you-run-a-python-script-as-a-service-in-windows

import win32serviceutil
import win32service
import win32event
import servicemanager
import socket
import time
from datetime import datetime as dt

# hosts_temp=r"D:\Dropbox\pp\block_websites\Demo\hosts"
# hosts_path="/etc/hosts"                               #Mac & Linux
hosts_path="C:\Windows\System32\drivers\etc\hosts"      #Windows
redirect="127.0.0.1"
website_list=["www.facebook.com","facebook.com","www.gmail.com","www.outlook.com"]


class AppServerSvc (win32serviceutil.ServiceFramework):
    _svc_name_ = "TestService"
    _svc_display_name_ = "Test Service"

    def __init__(self,args):
        win32serviceutil.ServiceFramework.__init__(self,args)
        self.hWaitStop = win32event.CreateEvent(None,0,0,None)
        socket.setdefaulttimeout(60)

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_,''))
        self.main()

    def main(self):
        while True:
            if timeCheck():
                print("Working hours...")
                with open(hosts_path,'r+') as file:
                    content=file.read()
                    for website in website_list:
                        if website in content:
                            pass
                        else:
                            file.write(redirect+" "+ website+"\n")
            else:
                with open(hosts_path,'r+') as file:
                    content=file.readlines()
                    # print(content)
                    file.seek(0)
                    for line in content:
                        # print(line)
                        if not any(website in line for website in website_list):
                            file.write(line)
                    file.truncate()
                print("Fun hours...")
            time.sleep(5)



def timeCheck():
    return dt(dt.now().year,dt.now().month,dt.now().day,8) < dt.now() < dt(dt.now().year,dt.now().month,dt.now().day,16)



if __name__ == '__main__':
    win32serviceutil.HandleCommandLine(AppServerSvc)
