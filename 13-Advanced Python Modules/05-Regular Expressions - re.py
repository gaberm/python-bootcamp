"""Regular Expressions

Regular expressions are text-matching patterns described with a formal syntax. You'll often hear regular expressions referred to as 'regex' or 'regexp' in conversation. Regular expressions can include a variety of rules, from finding repetition, to text-matching, and much more. As you advance in Python you'll see that a lot of your parsing problems can be solved with regular expressions (they're also a common interview question!)."""

import re
import timeit

# List of patterns to search for
patterns = ['term1', 'term2']
patternsRE = re.compile(['term1', 'term2'])

# Text to parse
text = 'This is a string with term1, but it does not have the other term.'

for pattern in patterns:

    print('Searching for "%s" in:\n "%s"\n' %(pattern,text))

    #Check for match
    if re.search(pattern,text):
        print('Match was found. \n')
    else:
        print('No Match was found.\n')

def reSearch(pattern, text):

    # for pattern in patterns:
    #     print('Searching for "%s" in:\n "%s"\n' %(pattern,text))

    #Check for match
    if re.search(pattern,text):
        print('Match was found. \n')
    else:
        print('No Match was found.\n')

match = re.search('term1', text)
type(match)
match.start()
match.end()
match.span()
match.group()
match.groupdict()
match.regs
match.re

# Split with regular expressions
# Term to split on
split_term = '@'
phrase = 'What is the domain name of someone with the email: hello@gmail.com'

# Split the phrase
re.split(split_term,phrase)
phrase.split('@') == re.split(split_term,phrase)

re.findall('term', 'This is a string with term1, but it does not have the other term.')     # return all matches
re.search('term', 'This is a string with term1, but it does not have the other term.')      # return first match

"""re Pattern Syntax
This will be the bulk of this lecture on using re with Python. Regular expressions support a huge variety of patterns beyond just simply finding where a single string occurred.

We can use metacharacters along with re to find specific types of patterns."""

def multi_re_find(patterns,phrase):
    '''
    Takes in a list of regex patterns
    Prints a list of all matches
    '''
    print("The Phrase is :\n",phrase,"\n")
    for pattern in patterns:
        print('Searching the phrase using the re check: %r' %(pattern))
        print(re.findall(pattern,phrase))
        print('\n')

# List of patterns to search for
patterns = ['term1', 'term2']

# Text to parse
text = 'This is a string with term1, but it does not have the other term.'

multi_re_find(patterns, text)

"""Repetition Syntax

There are five ways to express repetition in a pattern:

    A pattern followed by the meta-character * is repeated zero or more times.
    Replace the * with + and the pattern must appear at least once.
    Using ? means the pattern appears zero or one time.
    For a specific number of occurrences, use {m} after the pattern, where m is replaced with the number of times the pattern should repeat.
    Use {m,n} where m is the minimum number of repetitions and n is the maximum. Leaving out n {m,} means the value appears at least m times, with no maximum.
"""

test_phrase = 'sdsd..sssddd...sdddsddd...dsds...dsssss...sdddd'

test_patterns = [ 'sd*',     # s followed by zero or more d's
                'sd+',          # s followed by one or more d's
                'sd?',          # s followed by zero or one d's
                'sd{3}',        # s followed by three d's
                'sd{2,3}',      # s followed by two to three d's
                ]

multi_re_find(test_patterns,test_phrase)


test_phrase = 'sdsd..sssddd...sdddsddd...dsds...dsssss...sdddd'

test_patterns = ['[sd]',    # either s or d
                's[sd]+']   # s followed by one or more s or d

multi_re_find(test_patterns,test_phrase)

test_phrase = 'This is a string!.. But it has\'  punctuation; How can we remove it?'
re.findall('[^!.\';? ]+',test_phrase)


"""Character Ranges

As character sets grow larger, typing every character that should (or should not) match could become very tedious. A more compact format using character ranges lets you define a character set to include all of the contiguous characters between a start and stop point. The format used is [start-end]."""

test_phrase = 'This is an example sentence. Lets see if we can find some letters. mo@gmail.com'
test_patterns=['[a-z]+',      # sequences of lower case letters
               '[A-Z]+',      # sequences of upper case letters
               '[a-zA-Z]+',   # sequences of lower or upper case letters
               '[A-Z][a-z]+', # one upper case letter followed by lower case letters
               '[a-zA-Z]+@[a-zA-Z]+.[a-zA-Z]+' ]  #search for email

multi_re_find(test_patterns,test_phrase)


"""Escape Codes

You can use special escape codes to find specific types of patterns in your data, such as digits, non-digits, whitespace, and more. For example:
Code 	Meaning
\d 	a digit
\D 	a non-digit
\s 	whitespace (tab, space, newline, etc.)
\S 	non-whitespace
\w 	alphanumeric
\W 	non-alphanumeric"""

test_phrase = 'This is a string with some numbers 1233 and a symbol #hashtag  mo@gmail.com'

test_patterns=[ r'\d+', # sequence of digits
                r'\D+', # sequence of non-digits
                r'\s+', # sequence of whitespace
                r'\S+', # sequence of non-whitespace
                r'\w+', # alphanumeric characters
                r'\W+', # non-alphanumeric
                ]

multi_re_find(test_patterns,test_phrase)
