import datetime

t = datetime.time(5,22,1)
print(t)
t.minute
t.microsecond
t.tzinfo
print (t.min)
print (t.max)
print (t.resolution)

t = datetime.date.today()
t.year
t.month
t.day
t = t.timetuple()
print( t )
print( datetime.date.resolution )

d1 = datetime.date(2015,3,11)
print(d1)
d2 = d1.replace(year = 2019)
print(d2)

d2 - d1

today = datetime.date.today()
print(today)
print('ctime:', today.ctime())
print('tuple:', today.timetuple())
print('ordinal:', today.toordinal())
print('Year :', today.year)
print('Month:', today.month)
print('Day  :', today.day)
