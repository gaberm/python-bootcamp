"""Collections Module

The collections module is a built-in module that implements specialized container data types providing alternatives to Python’s general purpose built-in containers. We've already gone over the basics: dict, list, set, and tuple."""

import collections

"""Counter
Counter is a dict subclass which helps count hashable objects. Inside of it elements are stored as dictionary keys and the counts of the objects are stored as the value."""

#Count lists
lst = [1,2,2,2,2,3,3,3,1,2,1,12,3,2,32,1,21,1,223,1]
collections.Counter(lst)

# Counter with strings
collections.Counter('aabsbsbsbhshhbbsbs')

# Counter with words.
s = 'How many times does each word show up in this sentence word times each each word'
words = s.split()
collections.Counter(words)
collections.Counter(words).keys()
collections.Counter(words).values()

collections.Counter(words).most_common()[::-1]

"""Common patterns when using the Counter() object

sum(c.values())                 # total of all counts
c.clear()                       # reset all counts
list(c)                         # list unique elements
set(c)                          # convert to a set
dict(c)                         # convert to a regular dictionary
c.items()                       # convert to a list of (elem, cnt) pairs
Counter(dict(list_of_pairs))    # convert from a list of (elem, cnt) pairs
c.most_common()[:-n-1:-1]       # n least common elements
c += Counter()                  # remove zero and negative counts   """


"""defaultdict

defaultdict is a dictionary-like object which provides all methods provided by a dictionary but takes a first argument (default_factory) as a default data type for the dictionary. Using defaultdict is faster than doing the same using dict.set_default method."""

d = {}
d['one']
# if passed with no object, behaves like a normal dictlist..?
d = collections.defaultdict()
d = collections.defaultdict(object)
d['one']

for item in d: print(item)

# initialize with default value.
d = collections.defaultdict(lambda: 0)
d['one']

for item in d: print(item)


"""OrderedDict

An OrderedDict is a dictionary subclass that remembers the order in which its contents are added"""


"""namedtuple

The standard tuple uses numerical indexes to access its members, for example:
"""
t = (12,13,14)
t[0]

Dog = collections.namedtuple('Dog','age breed name')
Cat = collections.namedtuple('Cat', 'fur claws name' )
c = Cat(fur='Fuzzy', claws = False, name = 'Kitty')
c.name = 'Doll'
sam = Dog(age=2,breed='Lab',name='Sammy')
frank = Dog(age=2,breed='Shepard',name="Frankie")
sam.age, sam[0]
sam.breed, sam[1]
sam.name
frank['age']
