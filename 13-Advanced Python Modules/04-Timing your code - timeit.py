import timeit

# For loop
timeit.timeit('"-".join(str(n) for n in range(100))', number=10000)

# List comprehension
timeit.timeit('"-".join([str(n) for n in range(100)])', number=10000)

# Map()
timeit.timeit('"-".join(map(str, range(200)))', number=10000)


# Let's repeat the above examinations using iPython magic!

%timeit "-".join(str(n) for n in range(100))
20.4 µs ± 269 ns per loop (mean ± std. dev. of 7 runs, 10000 loops each)

%timeit "-".join([str(n) for n in range(100)])
18.1 µs ± 56.2 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)

%timeit "-".join(map(str, range(100)))
14.4 µs ± 64.1 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
