"""StringIO

The StringIO module implements an in-memory file like object. This object can then be used as input or output to most functions that would expect a standard file object."""
from io import StringIO
from io import BytesIO


# Arbitrary String
message = 'This is just a normal string.'

# Use StringIO method to set as file object
StringIO.mro()
f = StringIO(message)

f.read()
f.write('\n Second line written to file like object')

# Reset cursor just like you would a file
f.seek(0)

f.read()


"""Text I/O

Text I/O over a binary storage (such as a file) is significantly slower than binary I/O over the same storage, because it requires conversions between unicode and binary data using a character codec. This can become noticeable handling huge amounts of text data like large log files. Also, TextIOWrapper.tell() and TextIOWrapper.seek() are both quite slow due to the reconstruction algorithm used.

StringIO, however, is a native in-memory unicode container and will exhibit similar speed to BytesIO. """
